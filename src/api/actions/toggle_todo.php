<?php
  require "../utils.php";

  if(isset($_POST) && isset($_POST["id"]) && isset($_POST["value"])){

    $db = connectDB();

    $id = $_POST["id"];
    $value = $_POST["value"];

    $query = "UPDATE Todo SET status = '$value' WHERE id = $id";
    pg_query($db, $query);

    closeDB($db);
    exit(json_encode(
      array(
        "message" => "Success"
      )
    ));
  }
  exit(json_encode(
    array(
      "message" => "Didn't receive POST data."
    )
  ));
?>
