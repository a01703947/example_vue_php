<?php
  require "../utils.php";

  if(isset($_POST) && isset($_POST["description"])){

    $db = connectDB();

    $desc = $_POST["description"];

    $query = "INSERT INTO Todo (description) VALUES ('$desc')";
    pg_query($db, $query);

    closeDB($db);
    exit(json_encode(
      array(
        "message" => "Success"
      )
    ));
  }
  exit(json_encode(
    array(
      "message" => "Didn't receive POST data."
    )
  ));
?>
