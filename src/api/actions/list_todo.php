<?php
  require "../utils.php";
  $db = connectDB();

  $query = "SELECT * FROM Todo;";

  $result = pg_query($db, $query);

  echo(
    json_encode(
      pg_fetch_all($result, PGSQL_ASSOC)
    )
  );

  closeDB($db);
?>
