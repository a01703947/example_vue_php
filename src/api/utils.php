<?php
  function connectDB(){
    return pg_connect("host=localhost port=5432 dbname=todo_example");
  }

  function closeDB($conn){
    pg_close($conn);
  }

  closeDB(connectDB());
?>
