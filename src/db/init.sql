CREATE TABLE Todo (
  ID SERIAL PRIMARY KEY,
  description TEXT,
  status BOOLEAN DEFAULT False
);
